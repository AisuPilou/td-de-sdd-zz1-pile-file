#include "Pile.h"

#ifndef DEF_FILE
#define DEF_FILE

#include "element.h"

typedef struct file
{
	int tailleMax;
	int tailleActuel;
	int debut;
	int fin;
	element *tab;
} MaillonF,*File;

File initialisationFile(int);
int estVideF(File);
void enfiler(File,element ,int * );
element defiler(File,int *);
void libererFile(File);
void affichage(element *,int);
#endif
