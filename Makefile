
CFLAGS = -Wall -Wextra -g -ansi -pedantic
LDFLAGS =

SRCS = $(wildcard *.c)
SRCDIR = src
OBJDIR = obj
BINDIR = bin


$(BINDIR)/tp2: $(patsubst %.c,$(OBJDIR)/%.o,$(SRCS))
	@mkdir -p $(@D)
	gcc $(LDFLAGS) -o $@ $^


$(OBJDIR)/%.o: %.h
$(OBJDIR)/%.o: %.c
	@mkdir -p $(@D)
	gcc -c $(CFLAGS) -o $@ $<
