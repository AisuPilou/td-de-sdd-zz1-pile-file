
#include <stdlib.h>
#include <stdio.h>
#include "Pile.h"


Pile initialisationPile(int taille)
{
	MaillonP * p = malloc(sizeof(MaillonP));
	
	if (p!=NULL)
	{
		p->tailleMax = taille;
		p->tailleActuel = 0;
		p->tab = malloc(sizeof(element)*taille);
		if (p->tab==NULL) 
		{
			free(p);
			p=NULL;
		}
	}
	return p;
}
int estVideP(Pile p)
{
	return (p->tailleActuel == 0);
}
void empilerP(Pile p,element el,int *code)
{
	*code = 1;
	if (p->tailleActuel<=p->tailleMax - 1)
	{
		p->tab[p->tailleActuel]= el;
		p->tailleActuel+=1;
		*code = 0;
	}
}
element depilerP(Pile p,int *code)
{
	element el=0;
	*code = 1;
	if(!estVideP(p))
	{
		p->tailleActuel-=1;
		el = p->tab[p->tailleActuel];
		*code =0;
	}
	return el;
} 
void libererPile(Pile p)
{
	free(p->tab);
	free(p);
	p=NULL;
}
