
#include <stdlib.h>
#include <stdio.h>
#include "File.h"

File initialisationFile(int taille)
{
	File f =(MaillonF*)malloc(sizeof(MaillonF));
	if (f != NULL)
	{
		f->tailleMax = taille;
		f->tailleActuel = 0;
		f->tab = (element*)malloc(sizeof(element)*taille);
		if (f->tab == NULL)
		{
			free(f);
			f = NULL;
		} 
		else 
		{
			f->debut = 0;
			f->fin = taille-1;
		}
	}
	return f;
}
int estVideF(File f)
{
	return (f->tailleActuel == 0);
}
void enfiler(File f,element v,int * code)
{
	*code = 1;
	if(f->tailleActuel < f->tailleMax)
	{
		if(estVideF(f))
		{
			f->fin=f->debut;
			f->tab[f->debut]=v;
		}
		else
		{
			f->fin=(f->fin+1)%f->tailleMax;
			f->tab[f->fin]=v;
		}
		*code = 0;
		f->tailleActuel ++;
	} 
}
element defiler(File f,int * code)
{
	element v = 0;
	*code = 1;
	if (estVideF(f)==0)
	{
		v = f->tab[f->debut];
		f->debut = (f->debut+1)%f->tailleMax;
		f->tailleActuel--;
		*code =0;
	}
	return v;
}
void libererFile(File f)
{
	free(f->tab);
	free(f);
	f=NULL;
}
void affichage(element *tab,int n)
{
	int i;
	for(i=0;i<n;++i)
	{
		printf("%d ",tab[i]);
	}
	printf("\n");
}
