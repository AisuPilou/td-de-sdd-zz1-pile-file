
#ifndef DEF_PILE
#define DEF_PILE

#include "element.h"

typedef struct pile
{
	int tailleMax;
	int tailleActuel;
	element *tab;
} MaillonP,*Pile;

Pile initialisationPile(int);
int estVideP(Pile );
void empilerP(Pile,element,int *);
element depilerP(Pile,int *);
void libererPile(Pile);

#endif
