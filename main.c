
#include <stdlib.h>
#include <stdio.h>
#include  "File.h"
#include "Pile.h"


void truc(int i, int n, element * T)
{
	int j,tmp;
	if (i == n)
	{
		for(j = 0;j < n;j++)
		{
			printf("%d ",T[j]);
		}
		printf("\n");
	}
	else
	{
		for(j=i; j<n; j++)
		{
			tmp=T[i]; T[i]=T[j]; T[j]=tmp;
			truc(i+1, n, T);
			tmp=T[i]; T[i]=T[j]; T[j]=tmp;
		}
	}
	
}
void swap(int * T,int i, int j)
{
	int tmp;
	tmp = T[i];
	T[i] = T[j];
	T[j] = tmp;
}
void truc_iteratif(int i, int n, int * T)
{
	int jl=i, il=i, Fin = 0, cd, test_pile = 1;
	Pile po = initialisationPile(n*n);
	if(po != NULL)
	{
		while (Fin == 0 && test_pile == 1)
		{
			if (il == n)
			{
				printf("T =");
				for (jl=0;jl<n;jl++)
				{
					printf("%d ",T[jl]);
				}
				printf("\n");
				if (!estVideP(po))
				{
					jl = depilerP(po, &cd);
					if(cd == 1) test_pile = 0;
					il = depilerP(po, &cd);
					if(cd == 1) test_pile = 0;
					swap(T,il,jl);
					jl++;
				}
				else
				{
					Fin = 1;
				}	
			}
			else
			{
				if (jl < n)
				{
					swap(T,il,jl);	
					empilerP(po, il, &cd);
					if (cd == 1) test_pile = 0;
					empilerP(po, jl, &cd);
					if (cd == 1) test_pile = 0;
					il++;
					jl = il;
				}
				else
				{
					if(!estVideP(po))
					{
						jl = depilerP(po, &cd);
						if(cd == 1) test_pile = 0;
						il = depilerP(po, &cd);
						if(cd == 1) test_pile = 0;
						swap(T,il,jl);
						jl++;
					}
					else
					{
						Fin = 1;
					}
				}
			}
		}
		if (test_pile == 0) printf("Erreur d'empilage ou de dépilage\n");
	}
	else printf("Erreur, La pile ne c'est pas allouer\n");
}
int main()
{
	int n = 3;
	int *T = malloc(sizeof(int)*n);
	int i,cd;
	Pile po = initialisationPile(4);
	for (i=0;i<n;i++)
	{
		T[i]=i;
		empilerP(po,i,&cd);
	}
	truc(0,n,T);
	printf("\n");
	truc_iteratif(0,n,T);
	return 0;
}
